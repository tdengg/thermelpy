import json
import numpy as np
from ThermElpy.tools.birch import EOS
from scipy.optimize import fmin, brent
from math import factorial as fc
from scipy.integrate import quad
from scipy.optimize import brentq
import matplotlib.pyplot as plt
try:
    import mpmath as mpm
except:
    print 'python library mpmath not found: using numerical procedure to calculate Debye function.'

class Debye(object):
    
    def __init__(self, fitmethod='birch'):
        
        
        
        self.__kb=8.6173303*10.**(-5.) #eV/K
        self.__h =4.135667662*10.**(-15.) #eV s
        self.__path = './'
        self.T = 1.
        self.__fitorder_EC = 1.2
        self.__fitorder_B = 3
        self.__fitorder_EOS = 6
        self.__m = 183.84 * 1.66053892173 * 10.**(-27.)#Atomic weight in kg
        self.__mod='B_const'
        self.__E_fname = '/eos.dat'
        self.__C_fname = '/Cij_0.json'
        self.__elastic = False
        
        self.__lt = False
        self.__natom=2.
        self.__thetaE = None
        
        self.__E0 = None
        self.__B0 = None
        self.__V0 = None
        self.__gamma = None
        #self.__fout_EC = open('out_EC','a')
        self.__fitmethod = fitmethod
        self.T_Deb = 300.
    
    def get_m(self):
        return self.__m
    def set_m(self, m):
        if m>0.9: self.__m = m * 1.66053892173 * 10.**(-27.)
        else: self.__m = m
    
    def get_thetaE(self):
        return self.__thetaE
    def set_thetaE(self, thetaE):
        self.__thetaE = thetaE
    def get_thetaDD(self):
        return self.__thetaDD
    def set_thetaDD(self, thetaDD):
        self.__thetaDD = thetaDD
    
    def get_numatom(self):
        return self.__natom
    
    def set_numatom(self, natom):
        self.__natom = natom
    
    def get_mod(self):
        return self.__mod
    
    def set_mod(self, mod):
        print 'setting mod: %s'%mod
        self.__mod =  mod
        
    def get_lt(self):
        return self.__lt
    
    def set_lt(self, lt):
        print 'setting low temperature correction: %s'%lt
        self.__lt =  lt
        
    def get_elastic(self):
        return self.__elastic
    
    def set_elastic(self, elastic):
        print 'elastic = %s'%elastic
        self.__elastic =  elastic 
    
    def get_fitorder_EC(self):
        return self.__fitorder_EC
    
    def set_fitorder_EC(self, fitorder):
        print 'setting fitorder'
        self.__fitorder_EC = fitorder
        
    def get_fitorder_EOS(self):
        return self.__fitorder_EOS
    
    def set_fitorder_EOS(self, fitorder):
        print 'setting fitorder'
        self.__fitorder_EOS = fitorder        
        
    def get_E_fname(self):
        return self.__E_fname
    
    def set_E_fname(self, E_fname):
        print 'setting E_fname'
        self.__E_fname =  E_fname
        
    def get_C_fname(self):
        return self.__C_fname
    
    def set_C_fname(self, C_fname):
        print 'setting C_fname'
        self.__C_fname =  C_fname
    
    def get_path(self):
        return self.__path
    
    def set_path(self, path):
        print 'setting path'
        self.__path = path
    
    def get_Cij(self):
        f=open(self.__path + self.__C_fname)
        self.__Cij_dic = json.load(f)
        f.close()
        return self.__Cij_dic
    
    def get_E0(self):
        return self.__E0
    def set_E0(self,E0):
        self.__E0 = E0
        
    def get_V(self):
        return self.__V
    def set_V(self,V):
        if V[0]>1.: V=np.array(V)*10.**(-30.)
        self.__V = V  
         
    def get_gamma(self):
        return self.__gamma
    def set_gamma(self,gamma):
        self.__gamma = gamma 
        
    def get_V0(self):
        return self.__V0
    def set_V0(self,V0):
        if V0>1.: V0=V0*10.**(-30.)
        print 'updating V0(%s) = %s'%(self.T,V0)
        self.__V0 = V0
    def get_B0(self):
        return self.__B0
    def set_B0(self,B0):
        print 'updating B0(%s) = %s'%(self.T,B0)
        self.__B0 = B0
    
    def calculate_moduli(self, scale):
        Cij_V = np.array(self.__Cij_dic[scale]['SM'])
        C = np.zeros((6,6))
        if not Cij_V.shape==(6,6):
            for j in range(6):
                for i in range(6):
                    C[i,j] = Cij_V[i+6*j]
        else:
            C=Cij_V
        
        BV = (C[0,0]+C[1,1]+C[2,2]+2.*(C[0,1]+C[0,2]+C[1,2]))/9.
        GV = ((C[0,0]+C[1,1]+C[2,2])-(C[0,1]+C[0,2]+C[1,2])+3.*(C[3,3]+C[4,4]+C[5,5]))/15.
        #EV = (9.*BV*GV)/(3.*BV+GV)
        #### calculate p-wave modulus ####
        EV = BV + 4./3.*GV
        #if self.__outmod == 'massiveoutput':
        #   self.__fout_EC.write(str(scale) +' '+ str(BV) +' '+ str(GV) +' '+ str(EV) + '\n')
        return BV, GV/BV, EV/BV, GV, EV
    
    def debye_function(self, x):
        exx = np.exp(-x)
        if x == 0: D=1.
        elif 0 < x <= 0.1:
            D = 1 - 0.375*x + x**2.*(0.05-5.952380953*10**(-4.)*x**2.)
        elif 0.1 < x <= 7.25:
            D = ( (((0.0946173*x-4.432582)*x+85.07724)*x-800.6087)*x+3953.632 ) / ( (((x+15.121491)*x+143.155337)*x+682.0012)*x+3953.632 )
        elif x > 7.25:
            N=int(25./x)
            
            D=0.
            D2=1.
            
            
            if N>1.:
                for i in range(1,N+1):
                    
                    DS = i
                    
                    x3 = DS*x
                    D2 = D2*exx
                    D = D + D2*( 6. + x3*(6. + x3*(3.+x3)) )/DS**4.
                
                    
            
                
            D = 3.*(6.493939402-D)/(x**3.)
        else: 
            print 'ERROR: Debye function out of bounds: D(%s)!'%(x)
        return D
    
    def debye_function_exp(self, x):
        Brillouin = [1./6.,-1./30.,1./42.,-1./30.,5./66.,-691./2730.,7./6.,-3617./510.,43867./798,-174611./330.]
        D2=0.
        for k in range(len(Brillouin)):
            D2 = D2+Brillouin[k]/( (2*(k+1)+3)*fc(2*(k+1)) )*x**(2*(k+1))
        return 1.- 3./8.*x + 3.*D2
    
    def debye_function_alalytic(self, x):
        
        return -1./5.*np.pi**4./x**4.-3./4.+3./x*np.log(1-np.exp(x))+9./x**2.*mpm.polylog(np.exp(x),2)-18./x**3.*mpm.polylog(np.exp(x),3)+18./x**4.*mpm.polylog(np.exp(x),4)
    
    def gamma_T(self, T_deb, T):
        F = np.exp( -(T_deb/(3.*T))**3. )
        return self.__gamma + 1/3.*F/(1+F)
    
    def debye_T(self, x):
        
        rho = self.__m/x
        rho0 = self.__m/self.__V0
        if self.__lt: 
            lowT_correction = np.real((x/self.__V0)**(1./3.*(np.exp(-self.T/self.T_Deb))))
            
        else: lowT_correction = 1.
        
        Const = self.__h/self.__kb* (3./(4.*np.pi))**(1./3.)
        
        if self.__mod=='prefactor': 
            c3= np.polyfit(self.__V, self.__B, self.__fitorder_B)
            p_B = np.poly1d(c3)
            theta = Const * 0.617 * (p_B(x)*10**(9.)/rho)**(1./2.) * x**(-1./3.)*(lowT_correction)
        elif self.__mod=='B_const': 
            theta0 = Const * 0.617 * (self.__B0*10**9./rho0)**(1./2.) * self.__V0**(-1./3.)*(lowT_correction)
            theta = theta0*(self.__V0/x)**self.gamma_T(theta0, self.T)
        elif self.__mod=='debug':
            B0=321.924303414506
            V0=15.7103029969695*10.**(-30)
            rho0=self.__m/V0
            gamma= 1.62296663327697
            theta0 = Const * 0.617 * (B0*10**9./rho0)**(1./2.) * V0**(-1./3.)*(lowT_correction)
            #print theta0,rho0,rho,V0,x,p_B(x)
            theta = theta0*(V0/x)**gamma
            
        return theta
    
    def free_energy(self,x):
        if x>1.: x=x*10.**(-30.)
        eos = EOS(self.__V*10.**30.,self.__E0)
        if self.__fitmethod == 'birch':
            eos.fit_birch()
            self.T_Deb = self.debye_T(x)
            E_x = eos.fbirch(x*10.**30.,eos.par[0],eos.par[1],eos.par[2],eos.par[3])
        elif self.__fitmethod == 'vinet':
            eos.fit_vinet()
            self.T_Deb = self.debye_T(x)
            E_x = eos.fvinet(x*10.**30.,eos.par[0],eos.par[1],eos.par[2],eos.par[3])
            
        return (E_x - ( self.debye_function(self.T_Deb/self.T) - 3.*np.log(1.-np.exp(-self.T_Deb/self.T)) ) * self.__kb * self.T + 9./8.*self.__kb*self.T_Deb)
        #return (p_E0(x) + (( -self.debye_function(self.T_Deb/self.T) + 3.*np.log(1.-np.exp(-self.T_Deb/self.T)) ) * self.__kb * self.T - 9./8.*self.__kb*self.T_Deb))

    def free_energyE(self,x):
        
        V0=[float(l)**3./self.__natom*10.**(-30.) for l in self.__l]
        c1= np.polyfit(V0, self.__E0, self.__fitorder_EOS)
        p_E0 = np.poly1d(c1)
        dp_E0 = np.polyder(p_E0)
        roots = np.roots(dp_E0)
        isreal = np.isreal(roots)
        i=0
        for val in isreal:
            if val: index = i
            
            i+=1
        
        if self.__V0 == None: self.__V0 = roots[index]
        
        self.T_Deb, self.thetaE = self.debye_T(x)
        
        return (p_E0(x) - 3*self.__kb*self.thetaE/2. - 3.*self.__kb*self.thetaE/(np.exp(self.thetaE/self.T)-1.))
        #return (p_E0(x) + (( -self.debye_function(self.T_Deb/self.T) + 3.*np.log(1.-np.exp(-self.T_Deb/self.T)) ) * self.__kb * self.T - 9./8.*self.__kb*self.T_Deb))


    def free_energyDD(self,x):
        self.__E0 = self.get_gsenergy()
        
        V0=[float(l)**3./self.__natom*10.**(-30.) for l in self.__l]
        c1= np.polyfit(V0, self.__E0, self.__fitorder_EOS)
        p_E0 = np.poly1d(c1)
        dp_E0 = np.polyder(p_E0)
        roots = np.roots(dp_E0)
        isreal = np.isreal(roots)
        i=0
        for val in isreal:
            if val: index = i
            
            i+=1
        
        if self.__V0 == None: self.__V0 = roots[index]
        
        self.T_Deb, self.thetaE = self.debye_T(x)
        func = lambda S: x**(1./3.)*self.__A**2./(2.*np.pi**2.*self.__C)*self.__h*S*(np.exp(S*(x)**(1./3.)/(self.__C*self.__A))-1.)**2.*np.exp(S*(x)**(1./3.)/(self.__C*self.__A))/(np.exp(self.__h*S/(self.__kb*self.T)))
        return (p_E0(x) - ( quad(self.Integrand,0.,self.__thetaDD*self.__kb/self.__h, args=(self.__h,self.__kb,self.__C,self.T,self.__V0,self.__A)) )[0] + 9./8.*self.__kb*self.T_Deb)

    
    def free_energy_vib(self,x):
        #E0 = self.get_gsenergy()
        
        
        #print self.__GoB
        #print -(  self.debye_function(self.debye_T(x)/self.T) + 3.*np.log(1.-np.exp(-self.debye_T(x)/self.T)) ) * self.__kb * self.T,9./8.*self.__kb*self.debye_T(x)
        #return +(   self.debye_function(self.debye_T(x)/self.T) + 3.*np.log(1.-np.exp(-self.debye_T(x)/self.T)) ) * self.__kb * self.T + 9./8.*self.__kb*self.debye_T(x)
        self.T_Deb = self.debye_T(x)
        return ( self.debye_function(self.T_Deb/self.T) - 3.*np.log(1.-np.exp(-self.T_Deb/self.T)) ) * self.__kb * self.T + 9./8.*self.__kb*self.T_Deb

    def free_energy_vibE(self,x):
        #E0 = self.get_gsenergy()
        
        
        #print self.__GoB
        #print -(  self.debye_function(self.debye_T(x)/self.T) + 3.*np.log(1.-np.exp(-self.debye_T(x)/self.T)) ) * self.__kb * self.T,9./8.*self.__kb*self.debye_T(x)
        #print self.__thetaE
        return  3*( self.__kb*self.__thetaE/2. - self.__kb*self.__thetaE/(np.exp(self.__thetaE/self.T)-1.))
    
    def free_energy_vibDD(self,x):
        #E0 = self.get_gsenergy()
        
        
        #print self.__GoB
        #print -(  self.debye_function(self.debye_T(x)/self.T) + 3.*np.log(1.-np.exp(-self.debye_T(x)/self.T)) ) * self.__kb * self.T,9./8.*self.__kb*self.debye_T(x)
        #print self.__C, x,self.__thetaDD*self.__kb/self.__h
        func = lambda S: x**(1./3.)*self.__A**2./(2.*np.pi**2.*self.__C)*self.__h*S*(np.exp(S*(x)**(1./3.)/(self.__C*self.__A))-1.)**2.*np.exp(S*(x)**(1./3.)/(self.__C*self.__A))/(np.exp(self.__h*S/(self.__kb*self.T)))
        U = ( quad(self.Integrand,0.,self.__thetaDD*self.__kb/self.__h, args=(self.__h,self.__kb,self.__C,self.T,self.__V0,self.__A)) )
        #print U
        return U

    def Integrand(self, X,h,kb,c,T, V, A):
        return V**(1./3.)*A**2./(2.*np.pi**2.*c)*h*X*(np.exp(X*(V)**(1./3.)/(c*A))-1.)**2.*np.exp(X*(V)**(1./3.)/(c*A))/(np.exp(h*X/(kb*T)))

        
    def optimization(self):
        #return brent(self.free_energy, brack=(15.5*10.**(-30.),17.*10.**(-30.)))
        return fmin(self.free_energy, 15.6, xtol=10.**(-35.))
    
    def find_min(self, listx, listy):
        minval = min(listy)
        minindex = listy.index(minval)
        m = listx[minindex]
        return m
    
    def calc_gamma_LT(self, Bp):
        self.__gamma = 1./2.*(Bp-1)
        return self.__gamma
    
    E0 = property(fget=get_E0, fset=set_E0)
    V = property(fget=get_V, fset=set_V)  
    V0 = property(fget=get_V0, fset=set_V0)
    B0 = property(fget=get_B0, fset=set_B0)
    m = property(fget=get_m, fset=set_m)
    gamma = property(fget=get_gamma, fset=set_gamma)
      
    path = property(fget=get_path, fset=set_path)
    fitorder_EC = property(fget=get_fitorder_EC, fset=set_fitorder_EC)
    fitorder_EOS = property(fget=get_fitorder_EOS, fset=set_fitorder_EOS)
    E_fname = property(fget=get_E_fname, fset=set_E_fname)
    C_fname = property(fget=get_C_fname, fset=set_C_fname)
    mod = property(fget=get_mod, fset=set_mod)
    lt = property(fget=get_lt, fset=set_lt)
    elastic = property(fget=get_elastic, fset=set_elastic)
    natom = property(fget=get_numatom, fset=set_numatom)
    thetaE = property(fget=get_thetaE, fset=set_thetaE)
    thetaDD = property(fget=get_thetaDD, fset=set_thetaDD)