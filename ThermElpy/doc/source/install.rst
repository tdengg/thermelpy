Download and installation
-------------------------

Download
^^^^^^^^



Go to `<https://bitbucket.org/tdengg/thermelpy/get/e839cfaea166.zip>`_ and download the zip file
or clone the git repository:
::
	git clone https://tdengg@bitbucket.org/tdengg/thermelpy.git
	
Install
^^^^^^^

Unzip and change to ThermElpy root directory where setup.py is located. Then execute
::

	python setup.py install
	
And for a local user installation (without root privileges):
::
	python setup.py install --prefix=~/YOUR_LOCAL_PYTHON_PATH
	
where '~/YOUR_LOCAL_PYTHON_PATH' is where the local python module will be installed.