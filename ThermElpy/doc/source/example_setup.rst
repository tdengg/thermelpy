Example script using ThermElpy
-----------------------------

The following example will show the procedure of calculating elastic constants (at T=0K) using either the GUI version or a *python* script.

a.) Calculate elastic tensor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Setup:
______

.. code-block:: python

	import numpy as np
	from ThermElpy.elatoms import Structures, ElAtoms
	from ThermElpy.io.vasp import POS
	
	########################## Read in POSCAR file: ######################
	poscar = POS('POSCAR').read_pos()
	
	###################### Create Structures instance: ###################
	structures = Structures('vasp')
	
	## Generate distorted structures and add them to structures object: ##
	atom = ElAtoms('vasp')
	atom.poscarToAtoms(poscar)
	for etan in np.linspace(-0.05,0.05,11):
		
		for strains in range(len(atom.strainList)):
			atom = ElAtoms('vasp')
			atom.poscarToAtoms(poscar)
			atom.distort(eta=etan, strainType_index = strains)
			structures.append_structure(atom)
			
	####################### Write vasp input files: #######################
	structures.write_structures(structures)
	
	#################### Start local vasp calculation: ####################
	structures.executable = '/home/t.dengg/bin/vasp/vasp.5.3/vasp'
	structures.calc_vasp()

Postprocessing:
_______________

.. code-block:: python

	from ThermElpy.elatoms import Structures, ElAtoms
	from ThermElpy.postprocess import ECs
	
	ec = ECs('vasp')
	ec.set_structures()
	ec.set_gsenergy()
	ec.set_analytics()
	
b.) Calculate thermal expansion using the Debye-Grüneisen model:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. code-block:: python

	import numpy as np
	import matplotlib.pyplot as plt
	
	from ThermElpy.elatoms import Structures, ElAtoms
	from ThermElpy.io.vasp import POS, Energy
	from ThermElpy.tools.birch import EOS 
	from ThermElpy.thermo.debye_main import Debye


	###########################################################
	#------------------- Setup EOS ---------------------------#
	#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

	poscar = POS('POSCAR').read_pos()
	structures = Structures('vasp')

	for etan in np.linspace(-0.1,0.1,11): #SET VOLUMETRIC STRAIN
    	atom = ElAtoms('vasp')
    	atom.poscarToAtoms(poscar)
    	atom.deform_volume(etan)
    	structures.append_structure(atom)

	structures.write_structures(structures)

	structures.executable = 'vasp_std'
	structures.calc_vasp()

	###########################################################
	#------------------ Evaluate EOS -------------------------#
	#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

	dic = structures.get_structures()
    
	V=[]
	E=[]
	for key in  sorted(dic):
	    engy = Energy(dic[key].get_path()+'/vasprun.xml')
    	engy.set_gsenergy()
    	V.append(key[1])
    	E.append(engy.get_gsenergy())
    
	eos = EOS(V,E)
	eos.fit_vinet()
	print V,E
	#### This is what we need as input for the DG model: ######
	V0 = eos.Vequi
	B0 = eos.B0
	gamma = eos.gamma_LT
	print V0, B0
	###########################################################
	#------------------- Free energy -------------------------#
	#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

	deb = Debye(fitmethod='vinet')

	deb.m = 50.942        
	deb.V = V                   
	deb.V0 = V0                            
	deb.E0 = E
	deb.B0 = B0
	deb.gamma = gamma

	Temps = np.linspace(0,5000,100)     #SET TEMPERATURE RANGE
	V_T = []
	for T in Temps:
    	deb.T = T
    	F=[]
    	for v in V: F.append(deb.free_energy(v))
    	eosTemp = EOS(V,F)
    	eosTemp.fit_vinet()
    	V_T.append(eosTemp.Vequi)
    	#coeff = np.polyfit(V, F, 6)
    	#p = np.poly1d(coeff)
    	#dp = np.polyder(p)
    	#V_T.append(np.roots(dp)[-1])
    
    
    
	###########################################################
	#--------- Coefficient of linear thermal expansion -------#
	#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
    
	alpha=[]
	for i,T in enumerate(Temps[0:-2]):
    	alpha.append(1/V0*(V_T[i+1]-V_T[i])/(Temps[i+1]-Temps[i])*10**(6)/3.)

	print alpha

	#plt.plot(Temps,V_T)
	plt.plot(Temps[0:-2],alpha)
