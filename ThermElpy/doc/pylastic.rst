ThermElpy package
================

Subpackages
-----------

.. toctree::

    ThermElpy.html
    ThermElpy.io
    ThermElpy.thermo
    ThermElpy.tools

Submodules
----------

ThermElpy.analyze module
-----------------------

.. automodule:: ThermElpy.analyze
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.calculate module
-------------------------

.. automodule:: ThermElpy.calculate
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.distort module
-----------------------

.. automodule:: ThermElpy.distort
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.elatoms module
-----------------------

.. automodule:: ThermElpy.elatoms
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.genFilestructure module
--------------------------------

.. automodule:: ThermElpy.genFilestructure
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.get_DFTdata module
---------------------------

.. automodule:: ThermElpy.get_DFTdata
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.objecthandler module
-----------------------------

.. automodule:: ThermElpy.objecthandler
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.postprocess module
---------------------------

.. automodule:: ThermElpy.postprocess
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.prettyPrint module
---------------------------

.. automodule:: ThermElpy.prettyPrint
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.spacegroup module
--------------------------

.. automodule:: ThermElpy.spacegroup
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.standard_setup module
------------------------------

.. automodule:: ThermElpy.standard_setup
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.status module
----------------------

.. automodule:: ThermElpy.status
    :members:
    :undoc-members:
    :show-inheritance:

ThermElpy.vaspIO module
----------------------

.. automodule:: ThermElpy.vaspIO
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ThermElpy
    :members:
    :undoc-members:
    :show-inheritance:
