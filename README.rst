ThermElpy
=========

A python package for calculating elastic constants from first principles.

derivate of `ElaStic <http://exciting-code.org/elastic/>`_

Installation
------------

Installation of Miniconda:
^^^^^^^^^^^^^^^^^^^^^^^^^^
::
  wget https://repo.anaconda.com/miniconda/Miniconda2-latest-Linux-x86_64.sh
  ./Miniconda2-latest-Linux-x86_64.sh
  source ~\.bashrc

Installation of python modules:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::
  conda install matplotlib
  conda install mpld3
  conda install lxml
  

clone ThermElpy git repository:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::
  cd to directory where thermelpy sources should be located
  git clone https://tdengg@bitbucket.org/tdengg/thermelpy.git

install ThermElpy module:
^^^^^^^^^^^^^^^^^^^^^^^^^
::
  cd thermelpy
  python setup.py install --prefix=~/.local (or your preffered path)
  add thermelpy root to your system $PATH or copy sgroup executable to you preffered path
