import numpy as np
from copy import copy
import json
import webbrowser
try:
    import matplotlib.pyplot as plt
    import mpld3
    
    mpl=True
except:
    mpl=False
import scipy.stats as stat

class CVS(object):
    def __init__(self, xdata, ydata):
        self.__strain = list(xdata)
        self.__energy = list(ydata)
    
    def calc_cvs(self, fitorder):
        
        
        
        strain = copy(self.__strain)
        energy = copy(self.__energy)
        
        self.__cvs = []
        
        while (len(strain) > fitorder+1):
            emax = max(strain)
            emin = min(strain)
            emax = max(abs(emin),abs(emax))
    
            S = 0
            for k in range(len(strain)):
                #Y      = energy[k]
                Y = np.polyfit(strain,energy, fitorder)[fitorder-2]*2.
                etatmp = []
                enetmp = []

                for l in range(len(strain)):
                    if (l==k): pass
                    else:
                        etatmp.append(strain[l])
                        enetmp.append(energy[l])
                
                Yfit = np.polyfit(etatmp,enetmp, fitorder)[fitorder-2]*2.
                #Yfit = np.polyval(np.polyfit(etatmp,enetmp, fitorder), strain[k])
                S    = S + (Yfit-Y)**2
            
            self.__cvs.append((np.sqrt(S/len(strain)),emax,fitorder))
            
    
            if (abs(strain[0]+emax) < 1.e-7):
                strain.pop(0)
                energy.pop(0)
            if (abs(strain[len(strain)-1]-emax) < 1.e-7):
                strain.pop()
                energy.pop()
                
        
        
        return self.__cvs
    
class ANALYTICS(object):
    def __init__(self, strain, E):
        self.__strain = strain
        self.__E = E
        self.__json1 =[]
        self.__json1_descriptor = []
    def pwcvs(self):
        if mpl:
            fig = plt.figure()
            ax1=fig.add_subplot(311)
            ax2=fig.add_subplot(312)
            ax3=fig.add_subplot(313)
        
        strain = self.__strain
        gsenergy = self.__E
        
        pwcvs = []
            
        j=0
        delta=0.
        m=0
        for fitorder in range(2,9,2):
            j=0
            marker=['+','o','d','s','<']
            ###########################################
            strain = copy(strain)
            energy = copy(gsenergy)
            CV_0=[]
            E2nd_0=[]
            while (len(strain) > fitorder+1):
                emax = max(strain)
                emin = min(strain)
                emax = max(abs(emin),abs(emax))
        
                S = 0
                for k in range(len(strain)):
                    Y      = energy[k]
                    etatmp = []
                    enetmp = []
        
                    for l in range(len(strain)):
                        if (l==k): pass
                        else:
                            etatmp.append(strain[l])
                            enetmp.append(energy[l])
                    coeff = np.polyfit(etatmp,enetmp, fitorder)
                    Yfit = np.polyval(coeff, strain[k])
                    S    = S + (Yfit-Y)**2
                        
                E2nd_0.append((coeff[fitorder-2],emax,fitorder))   
                CV_0.append((np.sqrt(S/len(strain)),emax,fitorder))
                cv=(np.sqrt(S/len(strain)),emax,fitorder)
        
                if (abs(strain[0]+emax) < 1.e-7):
                    strain.pop(0)
                    energy.pop(0)
                if (abs(strain[len(strain)-1]-emax) < 1.e-7):
                    strain.pop()
                    energy.pop()
            if mpl:
                ax1.plot([c[1] for c in E2nd_0],[c[0] for c in E2nd_0])
                ax2.plot([c[1] for c in CV_0],[c[0] for c in CV_0])
            ###################################################
            CVN=np.zeros((len(strain),100))
            CVmin= np.zeros(len(strain))
            while j < 100:
                
                CV = []
                E2nd=[]
                E_fluct = [e * (np.random.uniform(-1,1) * 0.000001*(1.+j/10) + 1.) for e in gsenergy]
                
                coeff = np.polyfit(strain, gsenergy, fitorder)
                poly = np.poly1d(coeff)
                dpoly = np.poly1d.deriv(poly)
                ddpoly = np.poly1d.deriv(dpoly)
        
            
                coeff_fluct = np.polyfit(strain, E_fluct, fitorder)
                poly_fluct = np.poly1d(coeff_fluct)
                dpoly_fluct = np.poly1d.deriv(poly_fluct)
                ddpoly_fluct = np.poly1d.deriv(dpoly_fluct)
            
                pvol = np.linspace(min(strain),max(strain),100)
                #print np.linalg.norm(poly_fluct(pvol)-poly(pvol))
                delta += np.linalg.norm(dpoly_fluct(pvol)-dpoly(pvol))
                
                
                strain = copy(strain)
                energy = copy(E_fluct)
                a=0
                while (len(strain) > fitorder+1):
                    
                    emax = max(strain)
                    emin = min(strain)
                    emax = max(abs(emin),abs(emax))
        
                    S = 0
                    for k in range(len(strain)):
                        Y      = energy[k]
                        etatmp = []
                        enetmp = []
        
                        for l in range(len(strain)):
                            if (l==k): pass
                            else:
                                etatmp.append(strain[l])
                                enetmp.append(energy[l])
                        coeff = np.polyfit(etatmp,enetmp, fitorder)
                        Yfit = np.polyval(coeff, strain[k])
                        S    = S + (Yfit-Y)**2
                    E2nd.append((coeff[fitorder-2],emax,fitorder))
                    CV.append((np.sqrt(S/len(strain)),emax,fitorder))
                    cv=(np.sqrt(S/len(strain)),emax,fitorder)
        
                    if (abs(strain[0]+emax) < 1.e-7):
                        strain.pop(0)
                        energy.pop(0)
                    if (abs(strain[len(strain)-1]-emax) < 1.e-7):
                        strain.pop()
                        energy.pop()
                              
                    CVN[a][j] = np.sqrt(S/len(strain))
                    a+=1
        
                j+=1
                if mpl:
                    ax1.plot([c[1] for c in E2nd],[c[0] for c in E2nd],marker[m])
                    ax2.plot([c[1] for c in CV],[c[0] for c in CV],marker[m])
            
            
                
            sumC=0.
            index=0
            for point in CVN:
                sumC+=point
                CVmin[index] = max(point)-min(point)
                
                index+=1
            
            m+=1
            
            
            
            CVmin = np.trim_zeros(CVmin)    
            array_var = np.array(CVmin)#list(reversed(CVmin)))
            array_CV = np.array([c[0] for c in CV_0])
            #print array_CV,array_var
            stability = array_CV**(1.)*array_var#/min(array_var)
            #ax3.plot(range(len(CVmin)),array_CV,':')
            #ax3.plot(range(len(CVmin)),array_var,'--')
            ax3.plot(range(len(CVmin)),stability)
        #plt.plot(pvol,)
        #plt.plot(pvol,poly(pvol),'--')
        
        
        
        #plt.show()

    def phist(self, Ndiv=100, Nsample=100, rand_dev=0.00001):
        
        eta = self.__strain
        gsenergy = self.__E
        
        pwCVS = []
        pwCVS_val = []
        fordr = []
        fetamax = []
        
        stddev = []
        hist_E2nd=[]
        j=0
        delta=0.
        m=0
        N=0
        E2nd_all = []
        # Dictionaries to be returned:
        self.__E2nd_0_dic ={}
        self.__CV_0_dic ={}
        self.__pwCVS_dic = {}
        
        #For html output:
        
        
        for fitorder in range(2,9,2):
            j=0
            ###########################################
            strain = copy(eta)
            energy = copy(gsenergy)
            CV_0=[]
            E2nd_0=[]
            while (len(strain) > fitorder+1):
                emax = max(strain)
                emin = min(strain)
                emax = max(abs(emin),abs(emax))
        
                S = 0
                for k in range(len(strain)):
                    Y      = energy[k]
                    etatmp = []
                    enetmp = []
        
                    for l in range(len(strain)):
                        if (l==k): pass
                        else:
                            etatmp.append(strain[l])
                            enetmp.append(energy[l])
                    coeff = np.polyfit(etatmp,enetmp, fitorder)
                    Yfit = np.polyval(coeff, strain[k])
                    S    = S + (Yfit-Y)**2
                        
                E2nd_0.append((coeff[fitorder-2],emax,fitorder))   
                E2nd_all.append((coeff[fitorder-2],emax,fitorder))
                
                CV_0.append((np.sqrt(S/len(strain)),emax,fitorder))
                
        
                if (abs(strain[0]+emax) < 1.e-7):
                    strain.pop(0)
                    energy.pop(0)
                if (abs(strain[len(strain)-1]-emax) < 1.e-7):
                    strain.pop()
                    energy.pop()
            
            self.__E2nd_0_dic[fitorder] = E2nd_0
            self.__CV_0_dic[fitorder] = CV_0
                
            ###################################################
            CVN=np.zeros((len(eta),Nsample))
            CVmin= np.zeros(len(eta))
            CVstd = np.zeros(len(eta))
            while j < Nsample:
                
                CV = []
                E2nd=[]
                E_fluct = [e * (np.random.uniform(-1,1) * rand_dev*(1.+j/10) + 1.) for e in gsenergy]
                
                coeff = np.polyfit(eta, gsenergy, fitorder)
                poly = np.poly1d(coeff)
                dpoly = np.poly1d.deriv(poly)
                ddpoly = np.poly1d.deriv(dpoly)
        
            
                coeff_fluct = np.polyfit(eta, E_fluct, fitorder)
                poly_fluct = np.poly1d(coeff_fluct)
                dpoly_fluct = np.poly1d.deriv(poly_fluct)
                ddpoly_fluct = np.poly1d.deriv(dpoly_fluct)
            
                pvol = np.linspace(min(eta),max(eta),100)
                
                delta += np.linalg.norm(dpoly_fluct(pvol)-dpoly(pvol))
                
                
                strain = copy(eta)
                energy = copy(E_fluct)
                a=0
                while (len(strain) > fitorder+1):
                    
                    emax = max(strain)
                    emin = min(strain)
                    emax = max(abs(emin),abs(emax))
        
                    S = 0
                    for k in range(len(strain)):
                        Y      = energy[k]
                        etatmp = []
                        enetmp = []
        
                        for l in range(len(strain)):
                            if (l==k): pass
                            else:
                                etatmp.append(strain[l])
                                enetmp.append(energy[l])
                        coeff = np.polyfit(etatmp,enetmp, fitorder)
                        Yfit = np.polyval(coeff, strain[k])
                        S    = S + (Yfit-Y)**2
                    E2nd.append((coeff[fitorder-2],emax,fitorder))
                    hist_E2nd.append(coeff[fitorder-2])
                    N+=1
                    CV.append((np.sqrt(S/len(strain)),emax,fitorder))
                    cv=(np.sqrt(S/len(strain)),emax,fitorder)
        
                    if (abs(strain[0]+emax) < 1.e-7):
                        strain.pop(0)
                        energy.pop(0)
                    if (abs(strain[len(strain)-1]-emax) < 1.e-7):
                        strain.pop()
                        energy.pop()
                              
                    CVN[a][j] = np.sqrt(S/len(strain))
                    a+=1
                    
                j+=1
                
                    
                
            sumC=0.
            index=0
            for point in CVN:
                sumC+=point
                CVmin[index] = (max(point)-min(point))**2.
                CVstd[index] = stat.tstd(point)
                index+=1
            
            
            print "Standard deviation of CVS of fitorder %s: %s"%(fitorder,stat.tstd(CVN))
            print "Standard deviation of d2E/d(eta)2 of fitorder %s: %s"%(fitorder,stat.tstd(hist_E2nd))
            self.standardev = stat.tstd(CVN)
            CVmin = np.trim_zeros(CVmin)   
            CVstd =  np.trim_zeros(CVstd) 
            array_var = np.array(CVmin)
            array_CV = np.array([c[0] for c in CV_0])
            
            stability = array_CV**(1.)*CVstd**(1.)
            
            stddev.append((fitorder,self.standardev))
            pwCVS.extend(stability)
            self.__pwCVS_dic[fitorder] = stability
            
            fordr.extend([fitorder for c in CV])
            fetamax.extend([c[1] for c in CV])
            
            m+=1
        
        
        hist_vect = self.make_hist(Ndiv,hist_E2nd)
        
        deltasum = 0
        for i in range(len(hist_vect)):
            
            if max(hist_vect[i-1],hist_vect[i])!=0: deltasum += abs(hist_vect[i-1]-hist_vect[i])/max(hist_vect[i-1],hist_vect[i])
        
        print deltasum/Ndiv
        
        x= np.linspace(min(hist_E2nd),max(hist_E2nd),Ndiv+1)
        
        pwCVS_copy = copy(pwCVS)
        pw_prediction=[]
        for i in range(3):
            pw_ind = pwCVS_copy.index(min(pwCVS_copy))
            
            for el in E2nd_all:
                if fordr[pw_ind]==el[2] and fetamax[pw_ind]==el[1] :
                    pw_prediction.append(el)
                    
            pwCVS_copy.pop(pw_ind)
                    
        hist_prediction  = x[hist_vect.index(max(hist_vect))]
        
        
        dist_x = np.linspace(stat.norm.ppf(0.001, loc=hist_prediction, scale = 0.1),stat.norm.ppf(0.999, loc=hist_prediction, scale = 0.1),100) 
        dist_y = stat.norm.pdf(dist_x, loc=hist_prediction, scale = 0.1)   
            
        best_prediction=0.
        for val in pw_prediction:
            if best_prediction==0. or abs(val[0]-hist_prediction)<(best_prediction-hist_prediction):
                best_prediction=val[0]
                best_eta = val[1]
                best_forder = val[2]
                
        print best_prediction,best_eta,best_forder
        print pw_prediction
        
        
        #return (best_eta,best_forder),pw_prediction, hist_prediction, best_prediction, fig, fig2
        return (best_eta,best_forder),pw_prediction, stddev, best_prediction, self.__pwCVS_dic, self.__CV_0_dic, self.__E2nd_0_dic
    
    def plot(self):
        fig, ax = plt.subplots(1,3)
        fig.set_size_inches(20,5)
        
        for key in sorted(self.__CV_0_dic.keys()):
            ax[0].plot([v[1] for v in self.__CV_0_dic[key]],[v[0] for v in self.__CV_0_dic[key]])
        for key in sorted(self.__E2nd_0_dic.keys()):
            ax[1].plot([v[1] for v in self.__E2nd_0_dic[key]],[v[0] for v in self.__E2nd_0_dic[key]])
        for key in sorted(self.__pwCVS_dic.keys()):
            ax[2].plot([v[1] for v in self.__CV_0_dic[key]],self.__pwCVS_dic[key])
        self.__json1.append(json.dumps(mpld3.fig_to_dict(fig)))
        self.__json1_descriptor.append('some description')
        
    def generate_html(self):
        htmls="""<script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script>
        <script type="text/javascript" src="http://mpld3.github.io/js/mpld3.v0.2.js"></script>
        <style>
        </style>
        """
        
        for i, nr in zip(range(len(self.__json1)), self.__json1_descriptor): 
            htmls+="<h2>%s</h2>"%nr
            htmls+="<div id='fig{0}'></div>".format(i)
            
        htmls+="""
        <script type="text/javascript">
            """
        for i, dic in zip(range(len(self.__json1)), self.__json1): htmls+="var json{0} = {1};\n".format(i,dic)
        for i, nr in zip(range(len(self.__json1)), self.__json1_descriptor):
            htmls+="mpld3.draw_figure('fig{0}', json{0});\n".format(i)
                
        htmls+="""</script>"""
        with open('out.html','w') as f:
            f.write(htmls)
            
    def load_html(self):
        webbrowser.open_new_tab('out.html')
            
        
    def make_hist(self, Ndiv, hist_E2nd):
        hist_vect=np.zeros(Ndiv+1)
        
        min_E2nd = min(hist_E2nd) 
        max_E2nd = max(hist_E2nd)
        d_E2nd = (max_E2nd - min_E2nd)/Ndiv
        hist_indices = [int((p-min_E2nd)/d_E2nd) for p in hist_E2nd]
        hist_vect = list(hist_vect)
        
        for ind in hist_indices:
            hist_vect[ind]+=1
        
        return hist_vect
    