'''
Created on Dec, 2017

@author: t.dengg
'''

from distutils.core import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='ThermElpy',
      version='0.1',
      description='Calculate ab-initio elastic constants.',
      url='http://github.com/tdengg/ThermElpy',
      author='Thomas Dengg',
      author_email='thomas.dengg@mcl.at',
      license='LGPL',
      packages=['ThermElpy', 'ThermElpy.tools','ThermElpy.tools.elDOS', 'ThermElpy.io', 'ThermElpy.thermo'],
      package_data={'ThermElpy':['templates/exciting2sgroup.xsl']},
      install_requires=['numpy','lxml','matplotlib','json','mpld3'],
      zip_safe=False)
